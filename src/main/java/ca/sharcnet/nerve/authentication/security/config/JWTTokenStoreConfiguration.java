package ca.sharcnet.nerve.authentication.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import java.security.KeyPair;

@Configuration
public class JWTTokenStoreConfiguration {
    private final String keystoreUrl;
    private final String keystorePassword;
    private final String keypairAlias;
    private final String signingKey;

    @Autowired
    public JWTTokenStoreConfiguration(@Value("${jwt.keystore_url:}") String keystoreUrl,
                                      @Value("${jwt.keystore_password:}") String keystorePassword,
                                      @Value("${jwt.keypair_alias:}") String keypairAlias,
                                      @Value("${jwt.shared_secret:}") String signingKey) {
        this.keystoreUrl = keystoreUrl;
        this.keystorePassword = keystorePassword;
        this.keypairAlias = keypairAlias;
        this.signingKey = signingKey;
    }

    @Bean
    @ConditionalOnProperty(
            value = "jwt.use_shared_secret",
            havingValue = "false",
            matchIfMissing = true
    )
    public JwtAccessTokenConverter jwtAccessTokenConverterKeyPair() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        FileSystemResource keystoreFile = new FileSystemResource(keystoreUrl);
        KeyStoreKeyFactory keyStoreFactory = new KeyStoreKeyFactory(keystoreFile, keystorePassword.toCharArray());
        KeyPair keyPair = keyStoreFactory.getKeyPair(keypairAlias);
        converter.setKeyPair(keyPair);
        return converter;
    }

    @Bean
    @ConditionalOnProperty(
            value = "jwt.use_shared_secret",
            havingValue = "true"
    )
    public JwtAccessTokenConverter jwtAccessTokenConverterSharedSecret() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(signingKey);
        return converter;
    }

    @Bean
    public TokenStore tokenStore(JwtAccessTokenConverter jwtAccessTokenConverter) {
        return new JwtTokenStore(jwtAccessTokenConverter);
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices(JwtAccessTokenConverter jwtAccessTokenConverter) {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore(jwtAccessTokenConverter));
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }
}
