package ca.sharcnet.nerve.authentication.security.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

@Configuration
public class JWTOAuth2Configuration extends AuthorizationServerConfigurerAdapter {

    private TokenStore tokenStore;
    private DefaultTokenServices tokenServices;
    private AuthenticationManager authenticationManager;
    private JwtAccessTokenConverter tokenConverter;
    private String clientId;
    private String encryptedClientSecret;

    @Autowired
    public JWTOAuth2Configuration(TokenStore tokenStore,
                                  DefaultTokenServices tokenServices,
                                  AuthenticationManager authenticationManager,
                                  JwtAccessTokenConverter tokenConverter,
                                  @Value("${client.id}") String clientId,
                                  @Value("${client.encrypted_secret}") String encryptedClientSecret) {
        this.tokenStore = tokenStore;
        this.tokenServices = tokenServices;
        this.authenticationManager = authenticationManager;
        this.tokenConverter = tokenConverter;
        this.clientId = clientId;
        this.encryptedClientSecret = encryptedClientSecret;
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(clientId)
                .secret(encryptedClientSecret)
                .authorizedGrantTypes("refresh_token", "password", "client_credentials")
                .scopes("nerve_client");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .tokenStore(tokenStore)
                .accessTokenConverter(tokenConverter)
                .tokenEnhancer(tokenConverter)
                .authenticationManager(authenticationManager);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer oauthServer) throws Exception
    {
        oauthServer.checkTokenAccess("permitAll()");
    }
}
