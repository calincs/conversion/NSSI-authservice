INSERT INTO users (username, password, enabled)
	  values ('admin',
	    '$2a$04$ATI3lL.aBgt6cxkX0VnaUeCsKf31XXt2NBmVq.gld8SW4Lda41Sj6',
	    1),
	         ('user1',
	    '$2a$04$3jilucCVUV1M0/blgrLKwOiGzl1/m6B6tuQNooNLBbE1UsMTiI7ni',
	    1),
	         ('user2',
	    '$2a$04$jrTKMMl7GUEdj9m5jvEtRuQUnFEHN0pfYt1ZIWRIKo7KC6O..CvQO',
	    1);

INSERT INTO authorities (username, authority)
	  values ('admin', 'ROLE_ADMIN'),
             ('user1', 'ROLE_USER'),
             ('user2', 'ROLE_USER');
