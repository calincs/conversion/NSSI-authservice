package ca.sharcnet.nerve.authentication;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(properties = {
		"CLIENT_ID=id",
		"CLIENT_ENCRYPTED_SECRET=password",
		"JWT_SIGNING_KEY=key"
})
@ActiveProfiles("dev")
@SpringBootTest
class NSSIAuthServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
